from pathlib import Path

from pydantic import SecretStr
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    bot_token: SecretStr

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")
    ROOT_DIR: str = str(Path(__file__).absolute().parent)
    SLR_SERVICE_API: str = 'http://84.201.129.222:8000'


config = Settings()
