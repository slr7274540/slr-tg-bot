FROM python:3.11
WORKDIR .
# install python dependencies
COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ENV LIBRARY_PATH=/lib:/usr/lib
COPY . .
