from aiogram.types import (InlineKeyboardButton, InlineKeyboardMarkup,
                           KeyboardButton, ReplyKeyboardMarkup)

SEND_CONTACT_KEYBOARD: ReplyKeyboardMarkup = ReplyKeyboardMarkup(
    row_width=1,
    resize_keyboard=True,
    keyboard=[[KeyboardButton(text="Поделиться номером", request_contact=True)]]
)

CONTACT_KEYBOARD: InlineKeyboardMarkup = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(
                text=text,
                callback_data=f'contact_btn_{callback}',
            ),
        ] for callback, text in {
            'send': 'Да, свяжитесь со мной',
            'dont_send': 'Нет, не хочу оставлять контакт',
        }.items()
    ]
)
