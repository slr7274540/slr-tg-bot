from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup

RATE_KEYBOARD: InlineKeyboardMarkup = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(
                text=text,
                callback_data=f'rate_btn_{callback}',
            ),
        ] for callback, text in {
            '5': 'Все понравилось😍',
            '4': 'Понравилось, но кое-что можно улучшить😁',
            '3': 'Пока не уверен😶',
            '2': 'Не удовлетворен результатом😐',
            '1': 'Больше не буду пользоваться сервисом😡',
        }.items()
    ]
)
