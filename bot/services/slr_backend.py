import requests
import time

from config_reader import config


def predict(video) -> str:
    files = {'video_file': open(video, 'rb')}
    response = requests.post(
        url=f'{config.SLR_SERVICE_API}/videos/predict',
        files=files,
        params={'model': 'mvit_v2_s'}
    )
    if response.status_code != requests.codes.ok:
        return None
    task_id = response.json()['task_id']
    complete = False
    max_poll = 0
    if response.status_code == requests.codes.ok:
        while not complete and max_poll != 15:
            response = requests.get(
                url=(
                    f'{config.SLR_SERVICE_API}/videos'
                    f'/prediction-processing-status/{task_id}'
                ),
            )
            if response.status_code != requests.codes.ok:
                return None
            response = response.json()
            if response['task_status'] == 'SUCCESS':
                complete = True
            else:
                max_poll += 1
                time.sleep(2)

        return response['task_result']['predictions']


def send_feedback(feedback: str, user_mail: str) -> str | None:
    response = requests.post(
        url=f'{config.SLR_SERVICE_API}/feedbacks/add',
        json={
            "usermail": user_mail,
            "feedback": feedback
        }
    )
    if response.status_code != requests.codes.ok:
        return None
    else:
        return 'OK'
