from aiogram import F, Router, types
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup

from bot.services import send_feedback


class FeedbackState(StatesGroup):
    waiting_feedback = State()


feedback_router = Router()


@feedback_router.message(FeedbackState.waiting_feedback)
async def feedback_handler(message: types.Message, state: FSMContext):
    response = send_feedback(
        feedback=message.text,
        user_mail=f'{str(message.from_user.id)}@gmail.com',
    )
    if response:
        await message.answer("Ваш отзыв отправлен разработчикам")
    else:
        await message.answer(
            "Возникла проблема при отправке отзыва, попробуйте еще раз позднее",
        )
    await state.clear()
