from aiogram import F, Router, types
from aiogram.fsm.context import FSMContext
from aiogram.fsm.state import State, StatesGroup
from aiogram.types import CallbackQuery

from bot.keyboards.contact import SEND_CONTACT_KEYBOARD


class ContactUsState(StatesGroup):
    deciding_to_send_contact = State()
    sending_contact = State()


contact_router = Router()


@contact_router.callback_query(
    ContactUsState.deciding_to_send_contact,
    F.data == 'contact_btn_dont_send',
)
async def contact_us__user_dont_send_contact(callback: CallbackQuery, state: FSMContext):
    await callback.message.answer(text="Будем ждать вашего письма!")
    await state.clear()


@contact_router.callback_query(
    ContactUsState.deciding_to_send_contact,
    F.data == 'contact_btn_send',
)
async def contact_us__user_send_contact(callback: CallbackQuery, state: FSMContext):
    await callback.message.answer(
        "Отправьте свой контакт",
        reply_markup=SEND_CONTACT_KEYBOARD,
    )
    await state.set_state(ContactUsState.sending_contact)


@contact_router.message(
    ContactUsState.sending_contact,
    F.contact,
)
async def user_contact(message: types.Message, state: FSMContext):
    await message.answer(
        f"Ваш номер: {message.contact.phone_number}",
        reply_markup=types.ReplyKeyboardRemove(),
    )
    await state.clear()
