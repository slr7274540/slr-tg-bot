import asyncio
import logging
import os
import random

from aiogram import Bot, Dispatcher, F, types
from aiogram.filters.command import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, FSInputFile

from bot.answers import BotAnswersTemplates
from bot.handlers import COMMANDS_DESCRIPTION
from bot.handlers.contact import ContactUsState, contact_router
from bot.handlers.feedback import feedback_router
from bot.keyboards.contact import CONTACT_KEYBOARD
from bot.handlers.feedback import FeedbackState
from bot.keyboards.rate_us import RATE_KEYBOARD
from bot.services import predict
from config_reader import config


logging.basicConfig(level=logging.INFO)


bot = Bot(token=config.bot_token.get_secret_value())
dp = Dispatcher()


async def menu():
    await bot.set_my_commands(
        [
            types.BotCommand(command=command, description=description)
            for command, description in COMMANDS_DESCRIPTION.items()
        ],
    )


@dp.message(Command("start"))
async def cmd_start(message: types.Message):
    await message.answer(
        BotAnswersTemplates.START.value,
        parse_mode='HTML',
    )


@dp.message(Command("feedback"))
async def cmd_feedback(message: types.Message, state: FSMContext):
    await message.answer(
        text=BotAnswersTemplates.FEEDBACK_WAIT.value,
    )
    await state.set_state(FeedbackState.waiting_feedback)


@dp.message(Command("get_example"))
async def cmd_get_example(message: types.Message):
    videos_path = os.path.join(config.ROOT_DIR, 'bot', 'temp_videos', 'examples')
    random_video_name = random.choice(
        [item for item in os.listdir(videos_path) if item != ".gitkeep"],
    )
    random_video = os.path.join(videos_path, random_video_name)
    await message.answer(
        f'Вот пример жеста <b>{random_video_name.split("_")[1]}</b>',
        parse_mode='HTML',
    )
    # работает быстрее чем send_video
    await message.answer_video(FSInputFile(random_video))


# если грузить без сжатия, до загрузится как документ
@dp.message(F.document)
async def get_doc(message: types.Message, bot: Bot):
    file_id = message.document.file_id
    file = await bot.get_file(file_id)
    video_name = f"video_{str(message.from_user.id)}.mp4"
    video_path = os.path.join(
        config.ROOT_DIR,
        'bot',
        'temp_videos',
        video_name,
    )
    await bot.download_file(file.file_path, video_path)
    await message.answer("Обработка видео...")
    model_res = predict(video_path)
    await message.answer(f"Предсказание - {model_res}")
    os.remove(video_path)


@dp.message(Command("get_video_prediction"))
async def get_video(message: types.Message):
    await message.answer('Пожалуйста, загрузите ваше видео')


@dp.message(Command("service_description"))
async def cmd_service_description(message: types.Message):
    await message.answer(
        BotAnswersTemplates.SERVICE_DESCRIPTION.value,
        parse_mode='HTML',
    )


@dp.message(Command("rate_us"))
async def cmd_rate_us(message: types.Message):
    await message.answer(
        text='Оцените ваше впечатление от взаимодействия с сервисом',
        reply_markup=RATE_KEYBOARD,
    )


@dp.callback_query(
    F.data.in_([f'rate_btn_{i}' for i in range(1, 6)]),
)
async def rate_us_buttons_press(callback: CallbackQuery):
    answer = (
        'Спасибо за вашу оценку! Сожалеем что вам не понравилось😔'
        if '2' in callback.data or '1' in callback.data
        else 'Спасибо за вашу оценку!🐥'
    )
    await callback.message.answer(answer)


@dp.message(Command("contact_us"))
async def cmd_contact_us(message: types.Message, state: FSMContext):
    await message.answer(
        text=BotAnswersTemplates.CONTACT_US.value,
        reply_markup=CONTACT_KEYBOARD,
        parse_mode='HTML',
    )
    await state.set_state(ContactUsState.deciding_to_send_contact)


async def main():
    dp.include_routers(contact_router, feedback_router)
    dp.startup.register(menu)
    await dp.start_polling(bot, skip_updates=True)


if __name__ == "__main__":
    asyncio.run(main())

